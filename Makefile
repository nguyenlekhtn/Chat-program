all: server client

CC = gcc
CFLAGS = -c -Wall
DEPS = header.h 
OBJ = server.o client.o

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -o $@ $< 

server: server.o header.o
	$(CC) -o $@ $^

client: client.o header.o
	$(CC) -o $@ client.o header.o

clean: 
	-rm -f  *.o *~ server client
