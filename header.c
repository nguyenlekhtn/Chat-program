#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>


#define BUFSIZE 256


void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
	
}

void sendAndRecv(int iii, int sockfd)
{
	char buf[BUFSIZE];
	if(iii == 0) // tin hieu tu ban phim
	{
		fgets(buf, BUFSIZE, stdin);
		buf[strcspn(buf, "\n")] = '\0';
		//printf("%s\n", buf);
		if(strcmp(buf, "quit") == 0) // neu quit thi dong chat
		{
			close(sockfd);
			exit(0);
		}
		else // nguoc lai thi gui cho server
		{
			
			if(send(sockfd, buf, sizeof buf, 0) == -1)
			{
				perror("loi send");
				exit(1);
			}
		}
		memset(buf, 0, sizeof buf); 
	} // END tin hieu tu ban phim
	else // tin hieu tu server
	{
		// printf("Tin nhan tu server\n");
		int nbytes;
		if((nbytes = recv(sockfd, buf, sizeof buf, 0)) == -1)
		{
			perror("loi recv");
			exit(2);
		}
		buf[nbytes] = '\0';
		printf("%s\n", buf);
		memset(buf, 0, sizeof buf); 
	}
}